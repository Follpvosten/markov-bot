use std::{fs, sync::Arc};

use markov::Chain;
use once_cell::sync::OnceCell;
use rand::{prelude::SliceRandom, thread_rng};
use serde::Deserialize;
use tbot::contexts::methods::Message;
use tokio::sync::Mutex;
use tracing::{error, info};
use tracing_subscriber::{prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt};

#[derive(Deserialize)]
struct Config {
    token: String,
    markov: MarkovConfig,
}
#[derive(Deserialize)]
struct MarkovConfig {
    order: usize,
    append: bool,
    files: Vec<String>,
}

struct State {
    chain: Mutex<Chain<String>>,
    words: Vec<String>,
}
impl State {
    async fn feed_str(self: Arc<Self>, s: &str) {
        self.chain.lock().await.feed_str(s);
        if let Err(e) = append_line(s).await {
            error!("Error saving chain: {:?}", e);
        }
    }
}

static FILE: OnceCell<String> = OnceCell::new();
async fn append_line(line: &str) -> Result<(), std::io::Error> {
    use tokio::{fs::OpenOptions, io::AsyncWriteExt};
    let file = FILE.get().unwrap();
    OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open(file)
        .await?
        .write_all(format!("{}\n", line).as_bytes())
        .await?;
    Ok(())
}

#[tokio::main]
async fn main() {
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(tracing_subscriber::EnvFilter::from_default_env())
        .init();
    info!("Beep boop");
    info!("Loading config...");
    let config_s = fs::read_to_string("config.toml").expect("failed to read config");
    let Config {
        token,
        markov: MarkovConfig {
            order,
            append,
            files,
        },
    } = toml::from_str(&config_s).expect("failed to parse config");

    info!("Feeding chain...");
    let mut chain = Chain::<String>::of_order(order);
    for file in &files {
        if let Err(e) = chain.feed_file(file) {
            error!("Error feeding file {}: {}", file, e);
        }
    }
    let file = files
        .into_iter()
        .next()
        .unwrap_or_else(|| "chain.txt".into());
    FILE.set(file).unwrap();

    info!("Reading words file...");
    let words: Vec<String> =
        serde_json::from_str(&fs::read_to_string("worte.json").expect("failed to read words.json"))
            .expect("failed to parse words.json");

    info!("Creating bot...");
    let mut bot = tbot::Bot::new(token).stateful_event_loop(State {
        chain: Mutex::new(chain),
        words,
    });
    bot.fetch_username()
        .await
        .expect("failed to fetch username");

    info!("Registering commands...");
    if append {
        info!(file = ?FILE.get(), "Note: Append mode is on, will write to file");
        bot.text(|ctx, state| async move {
            state.feed_str(&ctx.text.value).await;
        });
        bot.photo(|ctx, state| async move {
            if !ctx.caption.value.is_empty() {
                state.feed_str(&ctx.caption.value).await;
            }
        });
    }
    bot.commands(["g", "genstr"], |ctx, state| async move {
        let query = ctx.text.value.as_str();
        let chain = state.chain.lock().await;
        let generated = if query.is_empty() {
            chain.generate_str()
        } else {
            let res = chain.generate_str_from_token(query);
            if res.is_empty() {
                chain.generate_str()
            } else {
                res
            }
        };
        drop(chain);
        if let Err(e) = ctx.send_message(generated).call().await {
            error!("Error sending generated message: {:?}", e);
        }
    });
    bot.command("isekai", |ctx, state| async move {
        let thing = state
            .words
            .choose(&mut thread_rng())
            .map(String::as_str)
            .unwrap_or_default();
        let msg = format!("Wiedergeboren als {} in einer anderen Welt.", thing);
        if let Err(e) = ctx.send_message(msg).call().await {
            error!("Error sending generated message: {:?}", e);
        }
    });

    info!("Starting up bot");
    tokio::select! {
        res = bot.polling().start() => { res.unwrap(); }
        _ = tokio::signal::ctrl_c() => { info!("Ctrl-C received"); }
    };
}
