FROM rust:1-bullseye AS builder
WORKDIR /markov
COPY Cargo.toml Cargo.lock ./
COPY src ./src
RUN cargo build --release

FROM debian:bullseye-slim AS runner
RUN apt update \
    && apt full-upgrade -y \
    && apt install ca-certificates -y \
    && apt autoremove --purge -y \
    && rm -rf /var/lib/apt/lists/*
COPY --from=builder /markov/target/release/markov-bot /usr/bin/markov-bot
RUN useradd --create-home --home-dir /markov markov
COPY --chown=markov:markov worte.json /markov/

WORKDIR /markov
USER markov
ENTRYPOINT [ "/usr/bin/markov-bot" ]
